# bhreg

Toy project about fractions, doing the [(re)introduction to TDD][1] from 
[Joe Rainsberger][2] and his [World's Best Intro to TDD][3]

[1]: https://online-training.jbrains.ca/courses/wbitdd-01/lectures/133270
[2]: https://online-training.jbrains.ca/
[3]: https://online-training.jbrains.ca/p/wbitdd-01

