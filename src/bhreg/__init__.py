__version__ = '0.0.1a'


class Fraction(object):
    def __init__(self, numerator, denominator=1):
        factor = _gcd(numerator, denominator)
        self.numerator = int(numerator / factor)
        self.denominator = int(denominator / factor)

    def __str__(self):
        return '{!s}/{!s}'.format(self.numerator, self.denominator)

    def __add__(self, other):
        """
        Addition of two fractions.

        Args:
            other (bhreg.Fraction): Right-hand operand.

        Returns:
            bhreg.Fraction: Result Fraction.
        """
        common_denominator = self.denominator * other.denominator
        left_numerator = self.numerator * other.denominator
        right_numerator = other.numerator * self.denominator
        return Fraction(left_numerator + right_numerator,
                        common_denominator)


def _gcd(a, b):
    """
    Computes the greater common divisor of *a* and *b* using Euclid's
    algorithm.

    Args:
        a: Left operand.  It will be cast using the :class:`int` constructor.
        b: Right operand.  It will be cast using the :class:`int` constructor.

    Returns:
        int: Greater common divisor of a and b.

    Examples:

        >>> _gcd(7, 0)
        7
        >>> _gcd(0, 7)
        7
        >>> _gcd(2, 4)
        2
        >>> _gcd(99, 121)
        11
        >>> _gcd(121, 99)
        11
        >>> _gcd(2, 3)
        1
    """
    a = int(a)
    b = int(b)
    if not a:
        return b
    if not b:
        return a
    return _gcd(b, a % b)
