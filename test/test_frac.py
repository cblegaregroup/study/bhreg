import random

import pytest

from bhreg import Fraction, _gcd  # noqa


def test_parts_are_integers(random_fraction):
    assert isinstance(random_fraction.numerator, int), (
        'Numerator of {!s} should be an integer'.format(random_fraction))
    assert isinstance(random_fraction.denominator, int), (
        'Numerator of {!s} should be an integer'.format(random_fraction))


def test_parts_are_lowest_terms(random_fraction):
    assert _gcd(random_fraction.numerator,
                random_fraction.denominator) == 1, (
        '{!s} parts are not expressed as lowest terms')


def test_equation(equation):
    left, operator, right, assertion, expected = equation_to_definition(
        equation)

    actual = operator(left, right)
    assert assertion(actual, expected), 'Expected {!s}, got {!s}'.format(
        expected, actual)


equations = [
    '0 + 0 = 0',
    '1 + 0 = 1',
    '0 + 1 = 1',
    '1/3 + 1/3 = 2/3',
    '1/3 + 1/3 = 2/3',
    '1/3 + 1/2 = 5/6'
]


a_bunch_of_random_fractions = [
    Fraction(random.randint(0, 100), random.randint(0, 100))
    for _ in range(100)
]


@pytest.fixture(params=a_bunch_of_random_fractions)
def random_fraction(request):
    return request.param


@pytest.fixture(params=equations)
def equation(request):
    return request.param


def equation_to_definition(equation):
    left, op, right, eq, expected = equation.split(' ', maxsplit=5)

    ops = {
        '+': lambda x, y: x + y
    }

    eqs = {
        '=': lambda x, y: are_equals(x, y)
    }

    return frac(left), ops[op], frac(right), eqs[eq], frac(expected)


def frac(fraction_string):
    """
    Make a :class:`bhreg.Fraction` from a string.

    Args:
        fraction_string (str): A string representation of a fraction.

    Returns:
        :class:`bhreg.Fraction`: Properly constructed fraction.

    Examples:

        >>> frac('1/2').numerator
        1
        >>> frac('1/2').denominator
        2
        >>> frac('5').denominator
        1
        >>> frac('3').numerator
        3
        >>> frac(7).numerator
        7
        >>> frac(7).denominator
        1
    """
    args = str(fraction_string).split('/', maxsplit=2)
    return Fraction(*[int(arg) for arg in args])


def are_equals(left, right):
    """
    Make sure two fractions are equals

    Args:
        left (bhreg.Fraction): Left-hand operand.
        right (bhreg.Fraction): Right-hand operand.

    Returns:
        bool: Whether both fraction are equals
    """
    return (
        left.denominator == right.denominator
        and
        left.numerator == right.numerator
    )
